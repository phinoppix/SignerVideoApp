Signer Video App
================

This application is a front-end client for libVLC, specifically, version 2.2.1, and tailored for my signing friends in the ASL community.

Current capabilities include:

* Selecting a video automatically displays the video chapters
* Pausing and playing video, skipping 5secs forward or backward
* Video rendering on a separate playback window
* Support any video format that VLC can play.

Playback window shortcuts
* Spacebar : play/pause video
* ESC : escape fullscreen

More updates and documentation to follow!
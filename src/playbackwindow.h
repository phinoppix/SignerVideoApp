#ifndef PLAYBACKWINDOW_H
#define PLAYBACKWINDOW_H

#include <QWidget>
#include <QKeyEvent>
#include <QDebug>

namespace Ui {
class PlaybackWindow;
}

class PlaybackWindow : public QWidget
{
    Q_OBJECT

public:
    explicit PlaybackWindow(QWidget *parent = 0);
    ~PlaybackWindow();

    WId getRenderWndId();
    bool standby();

public slots:
    void fullscreen(bool yes);

protected:
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;
    void hideEvent(QHideEvent *) Q_DECL_OVERRIDE;

private:
    Ui::PlaybackWindow *ui;
    bool fullscrn;

signals:
    void requestPause();
    void windowHid(bool);
    void windowFullScreenMode(bool);
    //void windowBlackOut(bool black);
};

#endif // PLAYBACKWINDOW_H

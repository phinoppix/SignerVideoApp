#include <QApplication>
#include "home.h"
#include "mediaplaystatus.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qRegisterMetaType<MediaPlayStatus>();
    Home home;
    home.show();

    return a.exec();
}

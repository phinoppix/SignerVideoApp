#ifndef MEDIAPLAYSTATUS
#define MEDIAPLAYSTATUS

#include <QCoreApplication>

struct MediaPlayStatus
{
    long time;
    int chapter;
    long videoLength;
};

Q_DECLARE_METATYPE(MediaPlayStatus)

#endif // MEDIAPLAYSTATUS


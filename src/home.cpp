#include "home.h"
#include "ui_home.h"
#include <QDebug>
#include <QThread>
#include "globals.h"
#include "mediaplaystatus.h"
#include "utils.h"

int playRates[7] = {50, 60, 75, 100, 130, 170, 200};

Home::Home(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Home)
{
    ui->setupUi(this);

    QDir home = QDir::home();
    QString libfolder = "signerMedia";
    QString libPath = home.filePath(libfolder);
    this->sliding = false;

    home.mkpath(libfolder);
    fsmodel.setRootPath(libPath);

    ui->treeFiles->setModel(&fsmodel);
    ui->treeFiles->setRootIndex(fsmodel.index(libPath));
    ui->treeFiles->hideColumn(1);
    ui->treeFiles->hideColumn(3);

    // list controls
    QObject::connect(
                ui->treeFiles->selectionModel(),
                SIGNAL(selectionChanged(QItemSelection,QItemSelection)),
                this,
                SLOT(treeSelectionChanged(QItemSelection, QItemSelection)));

    QObject::connect(ui->listChapters, SIGNAL(clicked(QModelIndex)), this, SLOT(onListChapters_clicked(QModelIndex)));

    // init and bind playback window
    wndPlayback = new PlaybackWindow();
    player.setPlaybackWindow(reinterpret_cast<void*>(wndPlayback->getRenderWndId()));

    // control panel buttons
    QObject::connect(ui->btnPlay, SIGNAL(clicked(bool)), this, SLOT(onBtnPlay_clicked()));
    QObject::connect(ui->btnBack5secs, SIGNAL(clicked(bool)), this, SLOT(onBtnBack5secs_clicked()));
    QObject::connect(ui->btnFwd5secs, SIGNAL(clicked(bool)), this, SLOT(onBtnFwd5secs_clicked()));
    QObject::connect(ui->btnStandby, SIGNAL(clicked(bool)), this, SLOT(onBtnStandby_clicked()));

    //show playback window
    QObject::connect(ui->chkShowPlayback, SIGNAL(clicked(bool)), this, SLOT(onChkShowPlayback_clicked(bool)));
    QObject::connect(wndPlayback, SIGNAL(windowHid(bool)), ui->chkShowPlayback, SLOT(setChecked(bool)));
    QObject::connect(ui->chkShowPlayback, SIGNAL(toggled(bool)), ui->chkFullscreen, SLOT(setEnabled(bool)));

    //fullscreen
    QObject::connect(ui->chkFullscreen, SIGNAL(clicked(bool)), wndPlayback, SLOT(fullscreen(bool)));
    QObject::connect(wndPlayback, SIGNAL(windowFullScreenMode(bool)), ui->chkFullscreen, SLOT(setChecked(bool)));

    //sliders
    QObject::connect(ui->videoSlider, SIGNAL(sliderMoved(int)), this, SLOT(onVideoTime_sliderMoved(int)));
    QObject::connect(ui->videoSlider, SIGNAL(sliderPressed()), this, SLOT(onVideoTime_sliderPressed()));
    QObject::connect(ui->videoSlider, SIGNAL(sliderReleased()), this, SLOT(onVideoTime_sliderReleased()));
    QObject::connect(ui->playrateSlider, SIGNAL(valueChanged(int)), this, SLOT(onPlayRateChanged(int)));

    //other playback window functions
    QObject::connect(wndPlayback, SIGNAL(requestPause()), &player, SLOT(pause()));
    QObject::connect(&player, SIGNAL(mediaTimeChanged(MediaPlayStatus)), this, SLOT(mediaTimeChanged(MediaPlayStatus)));

    //Force state update
    ui->chkFullscreen->setEnabled(false);
}

Home::~Home()
{
    delete wndPlayback;
    delete ui;
}

void Home::treeSelectionChanged(const QItemSelection selected, const QItemSelection)
{
    QModelIndex index = selected.indexes().first();
    if (fsmodel.isDir(index)) return;

    QString file = fsmodel.filePath(index);

#ifdef _WIN32
    file = file.replace("/", "\\");
#endif

    qDebug() << "selection=" << file;
    this->list.clear();
    ui->listChapters->clear();

    player.openMedia(file.toLatin1().constData());
    player.getChapters(list);
    qDebug() << "chapters=" << list.length();

    int nchapters = list.length();
    for(int i = 0; i < nchapters; i++)
    {
        ui->listChapters->addItem(list[i]);
    }

    ui->videoSlider->setMaximum(player.getDuration());
}


void Home::onListChapters_clicked(const QModelIndex &index)
{
    player.setChapter(index.row());
}

void Home::onBtnBack5secs_clicked()
{
    player.skip(-5);
    qDebug() << "Back 5secs";
}

void Home::onBtnPlay_clicked()
{
    QIcon ico;
    if (player.isplaying()) {
        ico = QIcon(":/images/icons/play-button.png");
        player.pause();
    }
    else {
        ico = QIcon(":/images/icons/pause.png");
        player.play();
    }
    ui->btnPlay->setIcon(ico);
}

void Home::onChkShowPlayback_clicked(bool checked)
{
    if (checked)
        wndPlayback->show();
    else
        wndPlayback->hide();
}

void Home::onBtnFwd5secs_clicked()
{
    player.skip(5);
}

void Home::mediaTimeChanged(MediaPlayStatus status)
{
    int d = player.getDuration();
    QString timeLeft = QString("%1 / %2").arg(formatTime(d - status.time), formatTime(d));
    ui->lblRunTime->setText(formatTime(status.time));
    ui->lblTimeLeft->setText(timeLeft);

    resetChapterColor();
    setChapterSelected(status.chapter);
    if (!this->sliding)
    {
        //qDebug() << "slider.setValue " << status.time;
        ui->videoSlider->setValue(status.time);
    }

    //qDebug() << "home_timechanged, " << QString("time=%1, sliding=%2").arg(timeLeft).arg(this->sliding);
}

void Home::closeEvent(QCloseEvent *)
{
    player.stop();
    wndPlayback->close();
}

void Home::showEvent(QShowEvent *)
{
    int w = ui->treeFiles->width() / 2;
    ui->treeFiles->setColumnWidth(0, w);
}

void Home::onBtnStandby_clicked()
{
    if (player.isplaying())
        player.pause();

    QIcon ico;
    if (wndPlayback->standby())
        ico = QIcon(":/images/icons/switch-on.png");
    else
        ico = QIcon(":/images/icons/switch-off.png");
    ui->btnStandby->setIcon(ico);
}

void Home::resetChapterColor()
{
    int n = ui->listChapters->count();
    for (int i = 0; i < n; i++)
    {
        QListWidgetItem *item = ui->listChapters->item(i);
        item->setForeground(Qt::black);
        item->setBackgroundColor(Qt::white);
        item->font().setBold(false);
    }
}

void Home::setChapterSelected(int index)
{
    if (ui->listChapters->count() == 0) return;

    QListWidgetItem *item = ui->listChapters->item(index);
    item->setForeground(Qt::white);
    item->setBackgroundColor(Qt::black);
    item->font().setBold(true);
}

void Home::onVideoTime_sliderMoved(int n)
{
    qDebug() << "on_videoTime_sliderMoved " << n;
    player.setPosition(n);
}

void Home::onVideoTime_sliderPressed()
{
    this->sliding = true;
    qDebug() << "sliding=" << this->sliding;
}

void Home::onVideoTime_sliderReleased()
{
    this->sliding = false;
    qDebug() << "sliding=" << this->sliding;
}

void Home::onPlayRateChanged(int value)
{
    player.setSpeed(playRates[value]);
}

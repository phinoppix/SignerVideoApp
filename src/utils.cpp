#include "utils.h"
#include <QDebug>

QString formatTime(const long ntime)
{
    if (ntime == 0) return QString("0:0");
    short s = (ntime / 1000) % 60;
    short m = ((ntime / 1000) / 60) % 60;
    short h = (ntime / 1000) / 3600;

    //qDebug() << "formatTime(), m=" << m << ", s=" << s;

    if (h > 0)
        return QString("%1:%2:%3").arg(h, 2, 10, QChar('0')).arg(m, 2, 10, QChar('0')).arg(s, 2, 10, QChar('0'));
    else
        return QString("%1:%2").arg(m, 2, 10, QChar('0')).arg(s, 2, 10, QChar('0'));
}


#-------------------------------------------------
#
# Project created by QtCreator 2016-03-13T01:04:29
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SignerVideoApp
TEMPLATE = app

win32:INCLUDEPATH += C:\workspace\vlc-2.2.3\sdk\include
win32:LIBS += -L"C:\workspace\vlc-2.2.3\sdk\lib" -llibvlc -llibvlccore -lvlc -lvlccore

SOURCES += main.cpp\
    playercontroller.cpp \
    home.cpp \
    playbackwindow.cpp \
    globals.cpp \
    utils.cpp

HEADERS  += \
    playercontroller.h \
    home.h \
    playbackwindow.h \
    globals.h \
    mediaplaystatus.h \
    utils.h

FORMS    += \
    home.ui \
    playbackwindow.ui

RESOURCES += \
    images.qrc

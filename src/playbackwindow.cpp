#include "playbackwindow.h"
#include "ui_playbackwindow.h"

PlaybackWindow::PlaybackWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlaybackWindow)
{
    ui->setupUi(this);
    ui->lblStandby->hide();
    ui->lblStandby->setAlignment(Qt::AlignCenter);
}

PlaybackWindow::~PlaybackWindow()
{
    delete ui;
}

WId PlaybackWindow::getRenderWndId()
{
    return ui->frame->winId();
}

void PlaybackWindow::keyPressEvent(QKeyEvent *event) {
    int key = event->key();
    //qDebug() << "keyPressEvent, " << event->key();

    switch(key)
    {
    case Qt::Key_F10:
    case Qt::Key_Escape:
        fullscreen(false);
        break;
    case Qt::Key_Space:
        if (ui->frame->isHidden()) return;
        emit requestPause();
        break;
    default:
        qDebug() << "keyPressEvent exit";
        break;
    }
}

void PlaybackWindow::hideEvent(QHideEvent *)
{
    //qDebug() << "PlaybackWindow::hideEvent";
    emit windowHid(false);
}

void PlaybackWindow::fullscreen(bool yes)
{
    if (!this->isVisible()) return;
    if (yes)
    {
        this->showFullScreen();
        emit windowFullScreenMode(true);
    }
    else
    {
        this->showNormal();
        emit windowFullScreenMode(false);
    }
}

bool PlaybackWindow::standby()
{
    if (!this->isVisible()) return false;
    if (ui->frame->isHidden())
    {
        ui->frame->show();
        ui->lblStandby->hide();
        return true;
    }

    ui->lblStandby->show();
    ui->frame->hide();
    return false;
}

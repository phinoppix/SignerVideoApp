set OUTPUTFOLDER=%1
set QTDIRBIN=%2

echo QTDIRBIN=%QTDIRBIN%
echo OUTPUTFOLDER=%OUTPUTFOLDER%

%QTDIRBIN%\windeployqt %OUTPUTFOLDER%\SignerVideoApp.exe

echo Copy VLC files
xcopy ..\..\vlc-2.2.3\*.* %OUTPUTFOLDER%\ /S /E /Y
rmdir /S /Q %OUTPUTFOLDER%\helpers\
rmdir /S /Q %OUTPUTFOLDER%\skins\
rmdir /S /Q %OUTPUTFOLDER%\sdk\
rmdir /S /Q %OUTPUTFOLDER%\lua\
rmdir /S /Q %OUTPUTFOLDER%\NSIS\
rmdir /S /Q %OUTPUTFOLDER%\languages\
rmdir /S /Q %OUTPUTFOLDER%\locale\
rmdir /S /Q %OUTPUTFOLDER%\translations\
del %OUTPUTFOLDER%\vlc*.*
del %OUTPUTFOLDER%\NEWS.txt
del %OUTPUTFOLDER%\spad*.*


echo Cleaning up
if exist %OUTPUTFOLDER%\*.o del %OUTPUTFOLDER%\*.o
if exist %OUTPUTFOLDER%\*.cpp del %OUTPUTFOLDER%\*.cpp
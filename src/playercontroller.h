#ifndef PLAYERCONTROLLER_H
#define PLAYERCONTROLLER_H

#include <QObject>
#include "vlc/vlc.h"
#include <QThread>
#include "mediaplaystatus.h"


class PlayerController : public QObject
{
    Q_OBJECT
public:
    explicit PlayerController(QObject *parent = 0);
    ~PlayerController();

    void setPlaybackWindow(void *wnd);
    void openMedia(const char* szfile);
    void getChapters(QList<QString> &list);
    void play();
    bool isplaying();
    void setChapter(int chapter);
    void skip(int seconds);
    void _mediaPlayTimeChanged(long ntime);
    void stop();
    long getDuration();
    void setPosition(int time);

private:
    libvlc_instance_t *vlc;
    libvlc_media_t *media;
    libvlc_media_player_t *player;
    libvlc_event_manager_t *eventMgr;
    long duration;
    bool hasmedia;

    void vlc_callback(const libvlc_event_t*, void*);
    void registerCallbacks();
    void deregisterCallbacks();

signals:
    void mediaTimeChanged(MediaPlayStatus status);
    void mediaLoaded(MediaPlayStatus status);

public slots:
    void pause();
    void setSpeed(int percent);
};

#endif // PLAYERCONTROLLER_H

#include "widget.h"
#include "ui_widget.h"
#include <QDebug>


Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    player = new PlayerController(this);
    player->setPlaybackWindow(reinterpret_cast<void*>(ui->frame->winId()));
}

Widget::~Widget()
{
    delete player;
}

void Widget::mousePressEvent(QMouseEvent *) {
    qDebug() << "mousePressEvent";
}

void Widget::mouseDoubleClickEvent(QMouseEvent *) {
    qDebug() << "mouseDoubleClickEvent";
    if (player->isplaying())
        player->pause();
    else
        player->play();
}

void Widget::keyPressEvent(QKeyEvent *event) {
    qDebug() << "keyPressEvent";

    if (event->key() == Qt::Key_Escape)
        showNormal();
}

void Widget::on_pushButton_clicked()
{
    player->openMedia("C:\\Users\\aris\\QtVlcPlayer Library\\w_ASL_201601_02_r360P.m4v");
    player->play();
}

void Widget::on_pushButton_2_clicked()
{
    if (isFullScreen())
        showNormal();
    else
        showFullScreen();
    qDebug() << "on_pushButton_2_clicked";
}

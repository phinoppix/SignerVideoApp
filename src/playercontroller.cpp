#include "playercontroller.h"
#include <QDebug>
#include <QTimer>
#include "globals.h"

void vlcCallback(const libvlc_event_t*, void*);
bool force_stop_playback(false);

PlayerController::PlayerController(QObject *parent) : QObject(parent)
{
    const char * const vlc_args[] = {
        "--ignore-config",
        "--verbose=0"
    };

    vlc = libvlc_new(sizeof(vlc_args) / sizeof(vlc_args[0]), vlc_args);
    player = libvlc_media_player_new(vlc);
    eventMgr = libvlc_media_player_event_manager(player);

    libvlc_video_set_key_input(player, false);
    libvlc_video_set_mouse_input(player, false);
    registerCallbacks();
}

PlayerController::~PlayerController()
{
    if (eventMgr) {
        deregisterCallbacks();
    }
    if (player) libvlc_media_player_release(player);
    libvlc_release(vlc);
}

void PlayerController::setPlaybackWindow(void*wnd)
{
    libvlc_media_player_set_hwnd(player, wnd);
}

void PlayerController::openMedia(const char *szfile)
{
    //qDebug() << "openMedia=" << qPrintable(szfile);

    libvlc_media_t *media = libvlc_media_new_path(vlc, szfile);
    libvlc_media_player_set_media(player, media);
    libvlc_media_release(media);
    duration = 0;
    hasmedia = true;
}

void PlayerController::play()
{
    libvlc_media_player_play(player);
    //qDebug() << "play(), " << libvlc_media_player_is_playing(player);
}

void PlayerController::pause()
{
    libvlc_media_player_pause(player);
}

bool PlayerController::isplaying()
{
    return libvlc_media_player_is_playing(player);
}


void PlayerController::getChapters(QList<QString> &list) {
    //TODO: Pass a pointer to the container where to save the chapters information
    if (!libvlc_media_player_is_playing(player))
    {
        force_stop_playback = true;
        this->play();
        QThread::msleep(400);
        //this->pause();

        libvlc_track_description_t* tdesc = libvlc_video_get_chapter_description(player, 0);
        while(tdesc) {
            list.append(QString::fromUtf8(tdesc->psz_name));
            tdesc = tdesc->p_next;
        }

        duration = libvlc_media_player_get_length(player);
        qDebug() << "duration : " << duration;
    }
}

void PlayerController::registerCallbacks()
{
    libvlc_event_attach(eventMgr, libvlc_MediaPlayerTimeChanged, vlcCallback, this);
}

void PlayerController::deregisterCallbacks()
{
    libvlc_event_detach(eventMgr, libvlc_MediaPlayerTimeChanged, vlcCallback, this);
}

void PlayerController::setChapter(int chapter) {
    libvlc_media_player_set_chapter(player, chapter);
}

void PlayerController::skip(int seconds)
{
    libvlc_time_t time = libvlc_media_player_get_time(player);
    libvlc_media_player_set_time(player, time + (seconds*1000));
}

void PlayerController::_mediaPlayTimeChanged(long ntime)
{
    MediaPlayStatus status;
    status.time = ntime;
    status.chapter = libvlc_media_player_get_chapter(player);
    //qDebug() << "time=" << status.time << " chap=" << status.chapter;
    emit mediaTimeChanged(status);
}


void PlayerController::stop()
{
    libvlc_media_player_stop(player);
}

long PlayerController::getDuration()
{
    return duration;
}

void PlayerController::setPosition(int time)
{
    libvlc_media_player_set_time(player, time);
}

void PlayerController::setSpeed(int percent)
{
    float n = percent / 100.0f;
    qDebug() << "speed = " << n;
    libvlc_media_player_set_rate(player, n);
}

void vlcCallback(const libvlc_event_t *event, void *)
{
    switch(event->type)
    {
        case libvlc_MediaPlayerTimeChanged:
            //qDebug() << "time=" << event->u.media_player_time_changed.new_time;
            player._mediaPlayTimeChanged(event->u.media_player_time_changed.new_time);
            if (force_stop_playback && event->u.media_player_time_changed.new_time > 100)
            {
                force_stop_playback = false;
                player.pause();
            }
            break;
    }
}

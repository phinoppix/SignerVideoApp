#ifndef HOME_H
#define HOME_H

#include <QWidget>
#include <QFileSystemModel>
#include <QItemSelection>
#include "playbackwindow.h"
//#include "playercontroller.h"
#include "mediaplaystatus.h"

namespace Ui {
class Home;
}

class Home : public QWidget
{
    Q_OBJECT

public:
    explicit Home(QWidget *parent = 0);
    ~Home();

public slots:
    void treeSelectionChanged(const QItemSelection selected, const QItemSelection deselected);

protected:
    void closeEvent(QCloseEvent *);
    void showEvent(QShowEvent *);

private:
    Ui::Home *ui;
    QFileSystemModel fsmodel;
    PlaybackWindow *wndPlayback;
    bool sliding;

    //PlayerController *player;
    QList<QString> list;
    void resetChapterColor();  // using a listView, item management is more flexible.
    void setChapterSelected(int index);


private slots:
    void onListChapters_clicked(const QModelIndex &index);
    void onBtnBack5secs_clicked();
    void onBtnPlay_clicked();
    void onChkShowPlayback_clicked(bool checked);
    void onBtnFwd5secs_clicked();
    void mediaTimeChanged(MediaPlayStatus status);
    void onBtnStandby_clicked();
    void onVideoTime_sliderMoved(int);
    void onVideoTime_sliderPressed();
    void onVideoTime_sliderReleased();
    void onPlayRateChanged(int value);
};

#endif // HOME_H
